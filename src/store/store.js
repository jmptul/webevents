import Vue from 'vue';
import Vuex from 'vuex';
import initialState from './initialState';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);
/* eslint-disable */
export default new Vuex.Store({
  state: initialState,
  mutations,
  actions,
});
