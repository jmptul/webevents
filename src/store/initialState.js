export default {
  userState: {
    name: '',
    description: '',
    numberEvents: 0,
    numberPlaces: 0,
    numberMessages: 0,
    numberFollowers: 0,
    earnings: 0,
    UID: 'UID',
    isLoading: false,
    events: [],
    places: [],
  },
};
