import firebase from 'firebase';
/* eslint-disable */
export default {
  setDataUser(state, payload) {
    state.userState.name = payload.name;
    state.userState.description = payload.description;
    state.userState.numberEvents = payload.number_events;
    state.userState.numberPlaces = payload.number_places;
    state.userState.UID = firebase.auth().currentUser.uid;
    state.userState.events = payload.events;
    state.userState.places = payload.places;
    state.userState.numberMessages = payload.number_messages;
    state.userState.numberFollowers = payload.number_followers;
    state.userState.earnings = payload.earnings;
  },
  setLoading(state, payload) {
    state.userState.isLoading = payload;
  },
};
