export default {
  setUser(context, user) {
    context.commit('setDataUser', user);
  },
  setLoading(context, isLoading) {
    context.commit('setLoading', isLoading);
  },
};
