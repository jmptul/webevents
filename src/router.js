import Vue from 'vue';
import firebase from 'firebase';
import Router from 'vue-router';
import Signin from './views/Signin.vue';
import MainScreen from './views/MainScreen.vue';
import Signup from './views/Signup.vue';
import AboutUs from './views/AboutUs.vue';
import GetACode from './views/GetACode.vue';
import AddEvent from './views/AddEvent.vue';
import AddPlace from './views/AddPlace.vue';
import InboxPage from './views/InboxPage.vue';
import ManageEvents from './views/ManageEvents.vue';
import ManagePlaces from './views/ManagePlaces.vue';
import Account from './views/Account.vue';
import EventDetails from './views/EventDetails.vue';
import PlaceDetails from './views/PlaceDetails.vue';
import AdminPanel from './views/AdminPanel.vue';
import AddOrganizer from './views/AddOrganizer.vue';
import AddPromotion from './views/AddPromotion.vue';
import ManagePromotions from './views/ManagePromotions.vue';
import DeleteOrganizer from './views/DeleteOrganizer.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/signin',
    },
    {
      path: '/manage_places',
      name: 'ManagePlaces',
      component: ManagePlaces,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/manage_promotions',
      name: 'ManagePromotions',
      component: ManagePromotions,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/add_organizer',
      name: 'AddOrganizer',
      component: AddOrganizer,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/delete_organizer',
      name: 'DeleteOrganizer',
      component: DeleteOrganizer,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/manage_events',
      name: 'ManageEvents',
      component: ManageEvents,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/event_details/:event',
      name: 'EventDetails',
      component: EventDetails,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/place_details/:place',
      name: 'PlaceDetails',
      component: PlaceDetails,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/inbox',
      name: 'InboxPage',
      component: InboxPage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/account',
      name: 'Account',
      component: Account,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/addevent',
      name: 'AddEvent',
      component: AddEvent,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/addplace',
      name: 'AddPlace',
      component: AddPlace,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/addpromotion',
      name: 'AddPromotion',
      component: AddPromotion,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/getacode',
      name: 'GetACode',
      component: GetACode,
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin,
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
    },
    {
      path: '/aboutus',
      name: 'AboutUs',
      component: AboutUs,
    },
    {
      path: '/main',
      name: 'main',
      component: MainScreen,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/admin-panel',
      name: 'admin-panel',
      component: AdminPanel,
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  // eslint-disable-next-line prefer-destructuring
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('/');
  else if (!requiresAuth && currentUser) next();
  else next();
});

export default router;
