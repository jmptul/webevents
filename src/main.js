/* eslint-disable import/prefer-default-export */
import Vue from 'vue';
import './plugins/vuetify';
import firebase from 'firebase';
import Vuetify from 'vuetify';
import * as VueGoogleMaps from 'vue2-google-maps';
import FlagIcon from 'vue-flag-icon';
import i18n from './plugins/i18n';
import App from './App.vue';
import router from './router';
import store from './store/store';


Vue.config.productionTip = false;
Vue.use(FlagIcon);
Vue.use(Vuetify);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBvrvKuOFHsMizEd9SYBBjllqWyfWlByLA',
    libraries: 'places',
  },
});

let app = '';

const config = {
  apiKey: 'AIzaSyCBso29Dp6dUwNd0XZnd1vz04DJwWqgWEA',
  authDomain: 'socialzit.firebaseapp.com',
  databaseURL: 'https://socialzit.firebaseio.com',
  projectId: 'socialzit',
  storageBucket: 'socialzit.appspot.com',
  messagingSenderId: '244826318749',
};
const appFire = firebase.initializeApp(config);
export const db = appFire.database();

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      i18n,
      store,
      router,
      render: h => h(App),
    }).$mount('#app');
  }
});
